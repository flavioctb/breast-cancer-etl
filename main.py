import petl
from akumen_api import get_results


def akumen(**kwargs):
    """
    Parameters:
        - Input: data [scenario]
        - Input: view [string]
        - Input: scope [string]

        - Output: results [file] (results.csv)
    """
    print('Running Akumen model...')
    
    # grab the whole resultset
    input = petl.fromdataframe(
        get_results(kwargs.get('data'), kwargs.get('view'), kwargs.get('scope'))
    )
    
    # reflow diagnosis into a 0/1 value, since autosklearn doesn't operate on labels
    input = petl.convert(input, 'diagnosis', lambda v: 0 if v == 'B' else 1)

    # strip off the akumen columns so we don't duplicate them
    for col in ['studyname', 'scenarioname', 'id']:
        input = petl.cutout(input, col)

    # or we can return the df directly to the `return` of the `akumen()` function:
    return {
        'results': petl.todataframe(input)
    }
